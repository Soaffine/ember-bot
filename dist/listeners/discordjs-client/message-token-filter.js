"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-try-statement */
const common_tags_1 = require("common-tags");
const discord_akairo_1 = require("discord-akairo");
class MessageTokenFilteringListener extends discord_akairo_1.Listener {
    constructor() {
        super("messageTokenFiltering", {
            emitter: "client",
            event: "message",
            category: "client",
        });
    }
    async exec(message) {
        try {
            const matches = /([\w-]+={0,2})\.([\w-]+={0,2})\.([\w-]+={0,2})/g.exec(message.content);
            return !matches ||
                BigInt(Buffer.from(matches ? matches[1] : "", "base64").toString()) ===
                    0n
                ? undefined
                : await message.channel
                    .send(common_tags_1.stripIndents `<@${message.author.id}>, the message you posted contained a bot token, you should reset it!
				> Go to <https://discordapp.com/developers/applications> and then click on the application that corresponds with your bot
				> Click "Bot" on the left side
				> Click the "Regenerate" button and then "Yes, do it!" on the popup.
				https://i.imgur.com/XtQsR9s.png`)
                    .then(() => !message.deletable
                    ? undefined
                    : message.delete({
                        reason: "Token Filtering: Message contained bot token",
                    }));
        }
        catch {
            return Promise.resolve();
        }
    }
}
exports.default = MessageTokenFilteringListener;
//# sourceMappingURL=message-token-filter.js.map