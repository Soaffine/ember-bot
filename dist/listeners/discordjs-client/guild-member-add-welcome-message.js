"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
require("dotenv/config");
/**
 * Defines how to announce a welcome message when users enter a guild.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class WelcomeMessageListener extends discord_akairo_1.Listener {
    /**
     * Emitted on {@link https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-guildMemberAdd | client.guildMemberAdd}
     */
    constructor() {
        super("welcomeMessage", {
            emitter: "client",
            category: "client",
            event: "guildMemberAdd",
        });
        this.welcomeChannel = process.env.welcomeChannel ?? ""; // TODO remove magic
        this.customBanner = process.env.customBanner ?? ""; // TODO remove magic
    }
    /**
     * Main execution procedure for the banner that can optionally be shown when a guild member is added.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param newMember - Will contain the new {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember}.
     * @returns If Settings.welcomeEnabled, a message sent to Settings.welcomeChannel set by admins through {@link WelcomeCommand}
     */
    async exec(newMember) {
        return (newMember.guild.channels.resolve(this.welcomeChannel) ?? // TODO make this load the welcome channel from settings
            newMember.guild.systemChannel)?.send(
        // TODO make this conditionally load the welcome message from settings
        `Hey <@${newMember.id}>. Welcome to the **${newMember.guild.name}** Discord.${newMember.guild.rulesChannel
            ? ` Read through the <#${newMember.guild.rulesChannelID ?? ""}> channel before engaging.`
            : ""}${newMember.guild.channels.cache.some((channel) => channel instanceof discord_js_1.NewsChannel)
            ? ` Check out <#${newMember.guild.channels.cache.findKey((channel) => channel instanceof discord_js_1.NewsChannel) ?? ""}> for community updates.`
            : ""} Enjoy the server!`, {
            files: [
                this.customBanner ??
                    newMember.guild.bannerURL() ??
                    newMember.guild.splashURL() ??
                    newMember.guild.discoverySplashURL() ??
                    newMember.guild.iconURL() ??
                    "src/assets/img/welcome.png",
            ],
        });
    }
}
exports.default = WelcomeMessageListener;
//# sourceMappingURL=guild-member-add-welcome-message.js.map