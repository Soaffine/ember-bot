"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
class DiscordErrorListener extends discord_akairo_1.Listener {
    constructor() {
        super("discordError", {
            emitter: "client",
            event: "error",
            category: "client",
        });
    }
    exec(error) {
        return this.client.logger.error(error.message, {
            topic: logger_1.TOPICS.DISCORD,
            event: logger_1.EVENTS.DEBUG,
        });
    }
}
exports.default = DiscordErrorListener;
//# sourceMappingURL=error-log.js.map