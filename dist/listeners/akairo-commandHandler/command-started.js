"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
/**
 * Defines how to handle a command being started.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class CommandStartedListener extends discord_akairo_1.Listener {
    constructor() {
        super("commandStarted", {
            emitter: "commandHandler",
            category: "commandHandler",
            event: "commandStarted",
        });
    }
    /**
     * Main execution procedure for handling {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler?scrollTo=e-commandStarted | a command starting}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param command - Will be the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | Command} called.
     * @param commandArguments - The set of arguments discovered by akairo.
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    async exec(message, command, commandArguments) {
        return message
            .react("⌛")
            .then(() => this.client.logger.verbose(`Started ${command.id} on ${message.guild ? `${message.guild.name} (${message.guild.id})` : "DM"}${Object.keys(commandArguments).length > 0
            ? ` with arguments ${JSON.stringify(commandArguments)}`
            : ""}`, { topic: logger_1.TOPICS.DISCORD_AKAIRO, event: logger_1.EVENTS.COMMAND_STARTED }));
    }
}
exports.default = CommandStartedListener;
//# sourceMappingURL=command-started.js.map