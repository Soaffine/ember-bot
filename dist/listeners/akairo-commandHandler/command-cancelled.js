"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
/**
 * Defines how to handle a command being cancelled.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class CommandCancelledListener extends discord_akairo_1.Listener {
    constructor() {
        super("commandCancelled", {
            emitter: "commandHandler",
            category: "commandHandler",
            event: "commandCancelled",
        });
    }
    /**
     * Main execution procedure for handling {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler?scrollTo=e-commandBlocked | a command being cancelled}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param command - Will be the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | Command} called.
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    exec(message, command) {
        return this.client.logger.verbose(`Cancelled ${command.id} on ${message.guild ? `${message.guild.name} (${message.guild.id})` : "DM"}`, { topic: logger_1.TOPICS.DISCORD_AKAIRO, event: logger_1.EVENTS.COMMAND_CANCELLED });
    }
}
exports.default = CommandCancelledListener;
//# sourceMappingURL=command-cancelled.js.map