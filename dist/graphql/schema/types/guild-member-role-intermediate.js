"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const GuildMemberRoleIntermediate = nexus_1.objectType({
    name: "GuildMemberRoleIntermediate",
    definition(t) {
        t.model.guildID();
        t.model.guildMember();
        t.model.guildMemberID();
        t.model.role();
        t.model.roleID();
        t.model.updatedAt();
        t.model.createdAt();
    },
});
exports.default = GuildMemberRoleIntermediate;
//# sourceMappingURL=guild-member-role-intermediate.js.map