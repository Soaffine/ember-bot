"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const Role = nexus_1.objectType({
    name: "Role",
    definition(t) {
        t.model.id();
        t.model.name();
        t.model.members();
        t.model.guild();
    },
});
exports.default = Role;
//# sourceMappingURL=role.js.map