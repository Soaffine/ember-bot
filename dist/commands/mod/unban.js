"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
class UnbanCommand extends bot_command_1.default {
    constructor() {
        super("unban", {
            aliases: ["unban", "ub"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["BAN_MEMBERS"],
            args: [{ id: "user", type: "string" }],
        });
    }
    async exec(message, { user }) {
        return !message.guild
            ? Promise.reject(new Error("This command must be run in a guild."))
            : message.guild
                .fetchBans()
                .then((bans) => bans.has(user)
                ? Promise.resolve()
                : Promise.reject(new Error("That member is not banned.")))
                .then(() => message.guild?.members.unban(user))
                .then((unbannedMember) => Promise.resolve(`${message.author.tag} has unbanned ${unbannedMember?.tag ?? ""}`));
    }
}
exports.default = UnbanCommand;
//# sourceMappingURL=unban.js.map