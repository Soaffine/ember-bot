"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const context_1 = require("../../graphql/context");
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
class RoleStoreCommand extends bot_command_1.default {
    constructor() {
        super("roleStore", {
            aliases: ["store"],
            category: "dev",
            ownerOnly: true,
            args: [{ id: "member", type: "member" }],
        });
    }
    /**
     * Main execution procedure for _store
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the Message object that hooked the command
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be banned.
     * @returns An updated guild prefix
     */
    async exec(message, { member }) {
        return !message.guild
            ? Promise.reject(new Error("This command must be run in a guild."))
            : context_1.prisma.guildMember
                .upsert({
                where: {
                    id_guildID: {
                        id: member.id,
                        guildID: member.guild.id,
                    },
                },
                select: {
                    id: true,
                    roles: true,
                },
                create: {
                    id: member.id,
                    guildID: member.guild.id,
                    roles: {
                        create: [
                            { role: { create: { id: "1", name: "test 1" } } },
                            { role: { create: { id: "2", name: "test 2" } } },
                        ],
                    },
                },
                update: {
                    roles: {
                        create: [
                            { role: { create: { id: "1", name: "test 1" } } },
                            { role: { create: { id: "2", name: "test 2" } } },
                        ],
                    },
                },
            })
                .then(() => Promise.resolve("Completed"));
    }
}
exports.default = RoleStoreCommand;
//# sourceMappingURL=role-store.js.map