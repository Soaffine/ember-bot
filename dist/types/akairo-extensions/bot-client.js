"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const path_1 = __importDefault(require("path"));
const graphql_client_1 = require("../../graphql/graphql-client");
const graphql_server_1 = require("../../graphql/graphql-server");
const logger_1 = require("../../logger");
// class PrefixQuery extends ApolloQueryResult<CachePrefix> {}
class BotClient extends discord_akairo_1.AkairoClient {
    constructor(config) {
        super({
            // Akairo Settings
            ownerID: config.owner,
        }, {
            // discord.js Settings
            disableMentions: "everyone",
            messageCacheMaxSize: 2048,
            partials: ["MESSAGE", "CHANNEL", "REACTION"],
        });
        this.inhibitorHandler = new discord_akairo_1.InhibitorHandler(this, {
            directory: path_1.default.join(__dirname, "../../", "inhibitors"),
        });
        this.listenHandler = new discord_akairo_1.ListenerHandler(this, {
            directory: path_1.default.join(__dirname, "../../", "listeners"),
        });
        this.commandHandler = new discord_akairo_1.CommandHandler(this, {
            directory: path_1.default.join(__dirname, "../../", "commands"),
            prefix: (message) => !message.guild
                ? "?"
                : graphql_client_1.graphQLClient
                    .query({
                    query: graphql_client_1.graphql.constants.READ_PREFIX,
                    variables: { id: message.guild.id },
                })
                    .then((data) => data.data.guild?.prefix ?? "?"),
            allowMention: true,
            commandUtil: true,
            handleEdits: true,
        })
            .useInhibitorHandler(this.inhibitorHandler)
            .useListenerHandler(this.listenHandler);
        this.logger = logger_1.logger;
        this.graphqlServer = graphql_server_1.graphqlServer;
        this.graphQLClient = graphql_client_1.graphQLClient;
        this.graphql = graphql_client_1.graphql;
        this.config = config;
        this.listenHandler.setEmitters({
            commandHandler: this.commandHandler,
            inhibitorHandler: this.inhibitorHandler,
            listenerHandler: this.listenHandler,
            process,
        });
        this.listenHandler.loadAll();
        this.logger.verbose("Listener Handler Loaded", {
            topic: logger_1.TOPICS.DISCORD_AKAIRO,
            event: logger_1.EVENTS.INIT,
        });
        this.inhibitorHandler.loadAll();
        this.logger.verbose("Inhibitor Handler Loaded", {
            topic: logger_1.TOPICS.DISCORD_AKAIRO,
            event: logger_1.EVENTS.INIT,
        });
        this.commandHandler.loadAll();
        this.logger.verbose("Command Handler Loaded", {
            topic: logger_1.TOPICS.DISCORD_AKAIRO,
            event: logger_1.EVENTS.INIT,
        });
        process.on("unhandledRejection", (error) => this.logger.error(`${error.message}\n === \n ${error.stack ?? ""}`, {
            topic: logger_1.TOPICS.UNHANDLED_REJECTION,
        }));
    }
    async start() {
        return this.login(this.config.token);
    }
}
exports.default = BotClient;
//# sourceMappingURL=bot-client.js.map