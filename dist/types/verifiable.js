"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
require("dotenv/config");
class Verifiable extends discord_js_1.GuildMember {
    constructor(client, member) {
        super(client, member, member.guild);
        this.isInVC = async (message) => {
            return Verifiable.isInVC(message, this.original);
        };
        this.isUnmuted = async (message) => {
            return Verifiable.isUnmuted(message, this.original);
        };
        this.isVerifiable = async (message) => {
            return Verifiable.isVerifiable(message, this.original);
        };
        this.original = member;
    }
}
exports.default = Verifiable;
Verifiable.admin = process.env.adminID ?? ""; // TODO remove magic
Verifiable.verificationVoice = process.env.verificationVoice ?? ""; // TODO remove magic
/**
 * Checks that the user has push to talk enabled.
 *
 * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
 * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be verified.
 * @returns A {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise | Promisified} {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} that is verifiable, or an {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error | Error}.
 */
Verifiable.isUnmuted = async function checkForPushToTalk(message, member) {
    return !message.member?.roles.cache.has(Verifiable.admin) &&
        member.voice.selfMute !== false
        ? Promise.reject(new Error(`${message.author.tag} attempted to verify ${member.user.tag} while they did not have push to talk enabled.`))
        : Promise.resolve(member);
};
/**
 * Checks that the user is in the proper voice chat for verification.
 *
 * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
 * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be verified.
 * @returns A {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise | Promisified} {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} that is verifiable, or an {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error | Error}.
 */
Verifiable.isInVC = async function checkForVerificationVoiceChat(message, member) {
    return !message.member?.roles.cache.has(Verifiable.admin) &&
        member.voice.channelID !== Verifiable.verificationVoice
        ? Promise.reject(new Error(`${message.author.tag} attempted to verify ${member.user.tag} who was not in the verification VC.`))
        : Promise.resolve(member);
};
Verifiable.isVerifiable = async (message, member) => {
    return Verifiable.isInVC(message, member).then(async () => Verifiable.isUnmuted(message, member));
};
//# sourceMappingURL=verifiable.js.map