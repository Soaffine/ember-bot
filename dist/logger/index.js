"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TOPICS = exports.EVENTS = exports.logger = void 0;
require("dotenv/config");
const winston_1 = require("winston");
const discord_transport_1 = __importDefault(require("./discord-transport"));
const events_1 = require("./events");
const null_transport_1 = __importDefault(require("./null-transport"));
const topics_1 = require("./topics");
// TODO https://www.npmjs.com/package/winston-daily-rotate-file2 if we end up using pm2
require("winston-daily-rotate-file");
exports.logger = winston_1.createLogger({
    levels: winston_1.config.cli.levels,
    format: winston_1.format.combine(winston_1.format.label({ label: `Ember Bot (${process.pid})` }), winston_1.format.errors({ stack: true })),
    transports: [
        new winston_1.transports.Console({
            format: winston_1.format.combine(winston_1.format.colorize({ level: true, message: true }), winston_1.format.timestamp({ format: "HH:mm:ss" }), winston_1.format.printf((info) => {
                const { timestamp, label, level, message, topic, event, ...rest } = info;
                return `\u001B[7m${level}\u001B[0m ${timestamp} from \u001B[2m\u001B[36m${label ?? ""}\u001B[0m for ${event ?? "NO EVENT GIVEN"} in ${topic ?? "NO TOPIC GIVEN"} ⇒ ${message} ${Object.keys(rest).length > 0 ? JSON.stringify(rest) : ""}`;
            })),
            level: process.env.NODE_ENV === "development" ? "verbose" : "debug",
        }),
        new winston_1.transports.DailyRotateFile({
            format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.json(), winston_1.format.timestamp({ format: "YYYY/MM/DD HH:mm:ss" })),
            level: "debug",
            dirname: "./logs/",
            filename: "Ember-%DATE%.log",
            maxFiles: "30d",
        }),
        process.env.NO_DISCORD
            ? new null_transport_1.default({})
            : new discord_transport_1.default({
                webhookID: process.env.webhookID ?? "",
                webhookToken: process.env.webhookToken ?? "",
                level: "info",
            }),
        new winston_1.transports.File({
            format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.json(), winston_1.format.timestamp({ format: "YYYY/MM/DD HH:mm:ss" })),
            filename: "debugging.log",
            dirname: "./logs/",
            level: process.env.NODE_ENV === "development" ? "silly" : "help",
        }),
    ],
    exceptionHandlers: [
        new winston_1.transports.File({ filename: "exceptions.log", dirname: "./logs/" }),
        new winston_1.transports.Console(),
    ],
    exitOnError: false,
});
exports.EVENTS = events_1.EVENTS;
exports.TOPICS = topics_1.TOPICS;
//# sourceMappingURL=index.js.map