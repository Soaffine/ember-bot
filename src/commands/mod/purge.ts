import { Message, GuildMember } from "discord.js";

import BotCommand from "../../types/akairo-extensions/bot-command";

/**
 * Defines how to respond to _purge.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
export default class PurgeCommand extends BotCommand {
  public constructor() {
    super("purge", {
      aliases: ["purge", "pg"],
      category: "mod",
      channel: "guild",
      clientPermissions: ["BAN_MEMBERS"],
      args: [{ id: "member", type: "member" }],
    });
  }

  /**
   * Main execution procedure for _purge.
   *
   * @remarks
   * This is required by Akairo
   *
   * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
   * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be purged.
   * @returns - A message indicating that the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} has been purged.
   */
  public async exec(
    message: Message,
    { member }: { readonly member: GuildMember },
  ): Promise<string> {
    return member
      .ban({ days: 7 })
      .then((bannedMember) =>
        Promise.resolve(
          `<@${message.author.id}> has purged **${bannedMember.user.tag}**`,
        ),
      );
  }
}
