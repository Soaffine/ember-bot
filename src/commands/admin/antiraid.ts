import { Message, GuildMember } from "discord.js";

import BotCommand from "../../types/akairo-extensions/bot-command";

/**
 * Defines how to respond to _antiraid.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
export default class BanCommand extends BotCommand {
  public constructor() {
    super("antiraid", {
      aliases: ["antiraid"],
      category: "admin",
      channel: "guild",
      clientPermissions: ["BAN_MEMBERS"],
      args: [
        {
          id: "first",
          type: "member",
        },
        {
          id: "last",
          type: "member",
        },
      ],
    });
  }

  /**
   * Main execution procedure for _antiraid.
   *
   * @remarks
   * This is required by Akairo
   *
   * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
   * @param first - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be the first member banned.
   * @param last - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be the last member banned.
   * @returns - A message indicating that the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} has been banned.
   */
  public async exec(
    message: Message,
    {
      first,
      last,
    }: { readonly first: GuildMember; readonly last: GuildMember },
  ): Promise<string> {
    return !message.guild
      ? Promise.reject(new Error("This command must be run in a guild."))
      : Promise.all(
          message.guild?.members.cache
            .filter((member) =>
              member.joinedTimestamp &&
              first.joinedTimestamp &&
              last.joinedTimestamp
                ? member.joinedTimestamp >= first.joinedTimestamp &&
                  member.joinedTimestamp <= last.joinedTimestamp &&
                  Date.now() - member.joinedTimestamp < 1800000
                : false,
            )
            .map((member) => member.ban()),
        ).then((handledMembers) =>
          Promise.resolve(
            `${message.author.toString()} has banned ${handledMembers.toString()}.`,
          ),
        );
  }
}
