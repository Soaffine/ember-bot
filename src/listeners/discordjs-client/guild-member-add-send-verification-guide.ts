import { Listener } from "discord-akairo";
import { TextChannel, MessageEmbed, Message, GuildMember } from "discord.js";
import "dotenv/config";

/**
 * Defines how to present a new {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember}.with a guide to becoming verified.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
export default class VerificationGuideListener extends Listener {
  private readonly verificationChannel = process.env.verificationChannel ?? ""; // TODO remove magic

  /**
   * Emitted on {@link https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-guildMemberAdd | client.guildMemberAdd}
   */
  public constructor() {
    super("verificationGuide", {
      emitter: "client",
      category: "client",
      event: "guildMemberAdd",
    });
  }

  /**
   * Main execution procedure for the optional verification guide.
   *
   * @remarks
   * This is required by Akairo
   *
   * @param newMember - Will contain the new {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember}.
   * @returns If Settings.verificationEnabled, a message sent to Settings.verificationChannel set by admins through {@link VerificationCommand}
   */
  public async exec(newMember: GuildMember): Promise<Message> {
    return newMember
      .send(
        new MessageEmbed()
          .setAuthor("Verification", newMember.guild.iconURL() ?? undefined)
          .setColor("RANDOM")
          .setDescription(
            "To interact with the server you must first become verified: \n" +
              '1.\na) For Desktop: Open "User Settings", find "Voice and Video", turn on "Push to Talk", and then record a push-to-talk key. \n' +
              "https://support.discord.com/hc/en-us/articles/21\n\n" +
              "b) For Mobile: Go to your voice chat settings and enable Push to Talk mode.\n\n" +
              "2. Make sure you have a functional microphone connected to your device, or that you are on a working mobile device.\n\n" +
              `3. Connect to the Verification voice chat that is just below the #${
                newMember.guild.channels.cache.get(this.verificationChannel)
                  ?.name ?? "verification"
              } channel.\n\n` +
              `4. Ping the Moderation team by typing \`@Moderator\` in the #${
                newMember.guild.channels.cache.get(this.verificationChannel)
                  ?.name ?? "verification"
              } channel.\n\n` +
              "5. Wait patiently. The moderation team is small and might not be around right now. Do not privately message the mods for verification, and please ensure that all steps are completed before pinging. \n" +
              "Note that pinging the moderators without fulfilling these will result in a ban.",
          )
          .setFooter("Do *not* DM this bot back, it cannot read your messages.")
          .setImage(
            newMember.guild.bannerURL() ??
              newMember.guild.splashURL() ??
              newMember.guild.discoverySplashURL() ??
              newMember.guild.iconURL() ??
              "",
          )
          .setThumbnail(
            newMember.guild.iconURL() ?? this.client.user?.avatarURL() ?? "",
          )
          .setTitle(`How to get verified in ${newMember.guild.name}`),
      )
      .catch(() =>
        (newMember.guild.channels.cache.get(
          this.verificationChannel,
        ) as TextChannel)
          .send(
            new MessageEmbed()
              .setAuthor("Verification", newMember.guild.iconURL() ?? undefined)
              .setColor("RANDOM")
              .setDescription(
                "To interact with the server you must first become verified: \n" +
                  '1.\na) For Desktop: Open "User Settings", find "Voice and Video", turn on "Push to Talk", and then record a push-to-talk key. \n' +
                  "https://support.discord.com/hc/en-us/articles/21\n\n" +
                  "b) For Mobile: Go to your voice chat settings and enable Push to Talk mode.\n\n" +
                  "2. Make sure you have a functional microphone connected to your device, or that you are on a working mobile device.\n\n" +
                  `3. Connect to the Verification voice chat that is just below the \`#${
                    newMember.guild.channels.cache.get(this.verificationChannel)
                      ?.name ?? "verification"
                  }\` channel.\n\n` +
                  `4. Ping the Moderation team by typing \`@Moderator\` in the \`#${
                    newMember.guild.channels.cache.get(this.verificationChannel)
                      ?.name ?? "verification"
                  }\` channel.\n\n` +
                  "5. Wait patiently. The moderation team is small and might not be around right now. Do not privately message the mods for verification, and please ensure that all steps are completed before pinging. \n" +
                  "Note that pinging the moderators without fulfilling these will result in a ban.",
              )
              .setFooter(
                "Do *not* DM this bot back, it cannot read your messages.",
              )
              .setImage(
                newMember.guild.bannerURL() ??
                  newMember.guild.splashURL() ??
                  newMember.guild.discoverySplashURL() ??
                  newMember.guild.iconURL() ??
                  "",
              )
              .setThumbnail(
                newMember.guild.iconURL() ??
                  this.client.user?.avatarURL() ??
                  "",
              )
              .setTitle(`How to get verified in ${newMember.guild.name}`),
          )
          .then((message) => message.channel.send(`<@${newMember.id}>`)),
      );
  }
}
